class ApplicationController < ActionController::API
  include ActionController::MimeResponds

  def respond_format(data)
    begin
      status = data[:status]
    rescue
      status = 200
    end
    respond_to do |format|
      format.json { render json: data, status: status }
      format.xml { render xml: data, status: status }
    end
  end

  def check_token
    @user = User.find_by(apiToken: params[:auth_token])
    respond_format({ status: 401, error: 'Invalid token' }) if !@user
  end

end
