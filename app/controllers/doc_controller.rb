class DocController < ApplicationController
  def index
    respond_to do |format|
      format.json { render json: 'Hello' }
      format.xml { render xml: 'Hello' }
    end
  end
end
