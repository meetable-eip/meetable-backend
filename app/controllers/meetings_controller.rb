class MeetingsController < ApplicationController
  before_action :check_token

  def index
    @organization = Organization.find(params[:organization_id])
    respond_format(@organization.meetings.map { |meeting| meeting.index_informations(@user) })
  end

  def show
    @meeting = Meeting.find(params[:id])
    if @meeting.users.include?(@user) || @meeting.admin == @user
      respond_format(@meeting.show_informations(@user))
    else
      respond_format({ status: 403, error: 'Forbidden' })
    end
  end

  def create
    @meeting = Meeting.new(meeting_params)
  end

  def update
  end

  def destroy
  end

  private

  def meeting_params
    params.permit(:name, :description, :beginDate, :endDate, :admin_id)
  end
end
