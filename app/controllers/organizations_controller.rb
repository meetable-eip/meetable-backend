class OrganizationsController < ApplicationController
  before_action :check_token
  before_action :check_correct_user, only: [:update, :destroy, :add_user, :delete_user]

  def index
    @organizations = @user.organizations.to_a
    @organizations.concat(@user.admin_orgas.to_a)
    respond_format(@organizations.map { |x| x.index_informations(@user) })
  end

  def show
    @organization = Organization.find(params[:id])
    if @organization.users.include?(@user) || @organization.admins.include?(@user)
      respond_format(@organization.show_informations)
    else
      respond_format({ status: 403, error: 'Forbidden' })
    end
  end

  def create
    @organization = Organization.new(organization_params)
    if @organization.save
      respond_format(@organization)
    else
      respond_format({ status: 400, error: 'Can not create organization' })
    end
    @organization.users << @user
    @organization.admins << @user
  end

  def update
    if @organization.update_attributes(organization_params)
      respond_format(@organization)
    else
      respond_format({ status: 400, error: 'Can not update organization' })
    end
  end

  def destroy
    @organization.destroy
  end

  def add_user
    @added_user = User.find_by(id: params[:user_id])
    if @added_user
      @organization.admins << @added_user if params[:admin] == 'true'
      @organization.users << @added_user
    else
      respond_format({ status: 400, error: 'User not found' })
    end
  end

  def delete_user
    @deleted_user = User.find_by(id: params[:user_id])
    if @deleted_user
      @organization.admins.delete(@deleted_user)
      @organization.users.delete(@deleted_user) unless params[:admin] == 'true'
    else
      respond_format({ status: 400, error: 'User not found' })
    end
  end

  private

  def organization_params
    params.permit(:name, :description)
  end

  def check_correct_user
    @organization = Organization.find(params[:id])
    if !@organization.admins.include?(@user) && !@user.admin
      respond_format({ status: 403, error: 'Not allowed to edit other organizations' })
      return
    end
  end
end
