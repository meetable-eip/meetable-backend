class UsersController < ApplicationController
  before_action :check_token, only: [:show, :update, :destroy]
  before_action :check_correct_user, only: [:update, :destroy]

  def index
    @users = User.all
    respond_format(@users.map { |user| user.index_informations })
  end

  def show
    @user = User.find(params[:id])
    respond_format(@user.show_informations) if @user
  end

  def create
    @user = User.new(user_params)
    if @user.save
      respond_format({ auth_token: @user.apiToken, id: @user.id })
    else
      respond_format({ status: 400, error: 'Can not create user' })
    end
  end

  def update
    if @user.update_attributes(user_params)
      respond_format(@user)
    else
      respond_format({ status: 400, error: 'Can not update user' })
    end
  end

  def destroy
    @user.destroy
  end

  def signin
    @user = User.auth(signin_params)
    if !@user.nil?
      respond_format({ auth_token: @user.apiToken, id: @user.id })
    else
      respond_format({ status: 400, error: "Can not login" })
    end
  end

  private

  def user_params
    params.permit(:login, :email, :password)
  end

  def signin_params
    params.permit(:login, :password)
  end

  def check_correct_user
    if User.find(params[:id]) != @user && !@user.admin
      respond_format({ status: 403, error: 'Not allowed to edit other users' })
      return
    end
  end

end
