class Meeting < ActiveRecord::Base
  has_and_belongs_to_many :users
  belongs_to :admin, class_name: 'User', foreign_key: 'admin_id'
  belongs_to :organization

  def index_informations(user = nil)
    {
      id: self.id,
      name: self.name,
      description: self.description,
      beginDate: self.beginDate,
      endDate: self.endDate,
      meeting_admin: (self.admin_id == user.id) ? true : false
    }
  end

  def show_informations(user = nil)
    index_informations(user).merge(
      {
        users: self.users.map { |user| user.index_informations }
      }
    )
  end
end
