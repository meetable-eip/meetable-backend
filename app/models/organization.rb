class Organization < ActiveRecord::Base
  has_and_belongs_to_many :users, before_add: :uniq_users
  has_many :meetings, before_add: :uniq_meetings
  has_many :organization_admins
  has_many :admins, through: :organization_admins, source: :user, before_add: :uniq_admins

  def uniq_meetings(meeting)
    raise ActiveRecord::Rollback if self.meetings.include? meeting
  end

  def uniq_users(user)
    raise ActiveRecord::Rollback if self.users.include? user
  end

  def uniq_admins(admin)
    raise ActiveRecord::Rollback if self.admins.include? admin
  end

  def user_informations
    {
      id: self.id,
      name: self.name,
      description: self.description,
    }
  end

  def index_informations(user = nil)
    user_informations.merge(
      {
        orga_admin: (self.admins.include?(user)) ? true : false
      }
    )
  end

  def show_informations
    index_informations.merge(
      {
        admins: self.admins.map(&:index_informations),
        users: self.users.map(&:index_informations),
      }
    )
  end
end
