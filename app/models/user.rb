class User < ActiveRecord::Base
  before_save :encrypt_password
  before_save :generateToken, if: :new_record?
  after_initialize :default_values

  has_and_belongs_to_many :meetings, before_add: :uniq_meetings
  has_and_belongs_to_many :organizations, before_add: :uniq_organizations
  has_many :organization_admins
  has_many :admin_orgas, through: :organization_admins, source: :organization, before_add: :uniq_admin_orgas

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :login, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: true, format: { with: email_regex }
  validates :password, presence: { if: :new_record? }

  def uniq_organizations(organization)
    raise ActiveRecord::Rollback if self.organizations.include? organization
  end

  def uniq_meetings(meeting)
    raise ActiveRecord::Rollback if self.meetings.include? meeting
  end

  def uniq_admin_orgas(organization)
    raise ActiveRecord::Rollback if self.admin_orgas.include? organization
  end

  def default_values
    self.admin ||= false
  end

  def index_informations
    {
      id: self.id, admin: self.admin, login: self.login
    }
  end

  def show_informations
    index_informations.merge({ organizations_admin: self.admin_orgas.map(&:user_informations),
                               organizations_member: self.organizations.map(&:user_informations) })
  end

  #SignUp
  def encrypt_password
    if self.changed.include?('password') && !password.blank?
      self.password = encrypt(password)
    else
      self.password = User.find(self.id).password
    end
  end

  def encrypt(string)
    secure_hash("nietnietniet-#{string}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end

  #SignIn
  def self.auth(params)
    user = User.find_by_login(params[:login])
    return nil if user.nil?
    return nil if user.password != user.encrypt(params[:password])
    user.generateToken
    user.save
    return user
  end

  def generateToken
    self.apiToken = self.encrypt("#{self.password}-#{self.id}-#{Time.now.nsec}").concat(Time.now.nsec.to_s)
  end

end
