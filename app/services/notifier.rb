class Notifier

  def initialize
    @channels = {}
    Thread.new {
      EventMachine.run {
        EventMachine::WebSocket.start(host: "0.0.0.0", port: 4096) do |ws|
          ws.onopen do
            sid = 0
            channelID = nil
            ws.onmessage do |msg|
              channelID = msg
              sid = @channels[channelID].subscribe { |msg| ws.send(msg) }
            end
            ws.onclose { @channels[channelID].unsubscribe(sid) }
          end
        end
      }
    }
  end

  def newChannel(id)
    @channels[id] = EM::Channel.new
  end

  def deleteChannel(id)
    @channels.delete(id)
  end

  def sendMessage(id, msg)
     @channels[id].push(msg)
  end

end
