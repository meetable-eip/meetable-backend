class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.string :name
      t.text :description
      t.datetime :beginDate
      t.datetime :endDate

      t.timestamps null: false
    end
  end
end
