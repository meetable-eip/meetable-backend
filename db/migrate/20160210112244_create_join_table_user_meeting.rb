class CreateJoinTableUserMeeting < ActiveRecord::Migration
  def change
    create_join_table :users, :meetings do |t|
      # t.index [:user_id, :organization_id]
      # t.index [:organization_id, :user_id]
    end
  end
end
