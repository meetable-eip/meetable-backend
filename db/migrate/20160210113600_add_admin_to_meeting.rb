class AddAdminToMeeting < ActiveRecord::Migration
  def change
    add_column :meetings, :admin_id, :integer
  end
end
