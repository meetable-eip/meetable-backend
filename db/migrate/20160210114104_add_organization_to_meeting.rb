class AddOrganizationToMeeting < ActiveRecord::Migration
  def change
    add_column :meetings, :organization_id, :integer
  end
end
